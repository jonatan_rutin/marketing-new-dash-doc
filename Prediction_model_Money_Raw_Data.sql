create or replace view DWH.MARKETING.PREDMOD_MONEY_RD_VIEW(
	USERID,
	GROWTH_COHORT_SDATE,
	COHORT_SDATE,
	PLATFORM_NAME,
	AD_ADV_NAME,
	ENGAGEINDEX,
	SPEND_PER_ATTRIBUTION,
	SPEND_PER_ATTRIBUTION_FIX,
	PRICE,
	NEW_TC,
	PURCHASE_DATE
) as
//create or replace temporary table DWH.MARKETING.predmod_money_rd_tt as
select a.userid,
       case when a.userid is null then a.cohort_sdate else first_value(a.cohort_sdate) over(partition by a.userid order by a.engageindex) end as growth_cohort_sdate, -- Spend without any attributions will be defined as growth_cohort_sdate
       a.cohort_sdate,
       a.platform_name,
       a.ad_adv_name,
       nvl(a.engageindex,0) engageindex, -- Spend without any attributions will be defined as growth
       a.SPEND_PER_ATTRIBUTION,
       a.spend_per_attribution/iff(count(price) over(partition by a.userid,a.engageindex)=0,1,count(price) over(partition by a.userid,a.engageindex)) SPEND_PER_ATTRIBUTION_fix,
       b.price,
       b.new_tc,
       b.purchase_date 
from "DWH"."MARKETING"."COHORT_USER_LEVEL_DATA" a
left join "DWH"."MARKETING"."COHORT_USER_PAYMENTS_GE" b
on a.userid = b.userid
and a.engageindex = b.engageindex
where a.cohort_sdate >= '2019-05-01'
and (a.userid not in(select distinct userid
                    from "DWH"."MARKETING"."COHORT_USER"
                    where install_ts < '2019-05-01')
    or a.userid is null
    )
order by a.userid,a.engageindex,purchase_date
;